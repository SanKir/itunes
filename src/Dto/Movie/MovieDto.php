<?php

declare(strict_types=1);

namespace App\Dto\Movie;

use DateTime;

class MovieDto
{
    public ?string $title = null;
    public ?string $description = null;
    public ?string $link = null;
    public ?string $image = null;
    public ?DateTime $pubDate = null;
}
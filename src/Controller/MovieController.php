<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Service\Movie\MovieService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Slim\Views\Twig;

class MovieController
{
    public function __construct(
        private Twig $twig,
        private EntityManagerInterface $em,
        private MovieService $movieService
    ) {}

    public function detail(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        try {
            $trailer = $this->em->getRepository(Movie::class)
                ->findOneBy(['id' => $args['id'] ?? null]);

            if (!$trailer) {
                throw new HttpNotFoundException($request);
            }

            $response = $this->twig->render($response, 'movie/detail.html.twig', [
                'trailer' => $trailer,
            ]);
        } catch (Exception $e) {
            if ($e instanceof HttpNotFoundException) {
                throw $e;
            }
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }

        return $response;
    }

    public function addLike(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        try {
            $trailer = $this->em->getRepository(Movie::class)
                ->findOneBy(['id' => $args['id'] ?? null]);

            if (!$trailer) {
                throw new HttpNotFoundException($request);
            }

            $this->movieService->addLike($trailer);
        } catch (Exception $e) {
            if ($e instanceof HttpNotFoundException) {
                throw $e;
            }
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }

        return $response->withStatus(200);
    }
}
<?php

declare(strict_types=1);

namespace App\Service\Movie;

use App\Dto\Movie\MovieDto;
use App\Entity\Movie;
use Doctrine\ORM\EntityManagerInterface;

class MovieService
{
    public function __construct(
        private EntityManagerInterface $em
    ) {}

    public function updateMovie(Movie $movie, MovieDto $movieDto): Movie
    {
        $movie
            ->setTitle($movieDto->title)
            ->setDescription($movieDto->description)
            ->setLink($movieDto->link)
            ->setImage($movieDto->image)
            ->setPubDate($movieDto->pubDate);

        $this->em->persist($movie);
        $this->em->flush();

        return $movie;
    }

    public function addLike(Movie $movie): Movie
    {
        $movie->setLikeCount($movie->getLikeCount() + 1);
        $this->em->persist($movie);
        $this->em->flush();

        return $movie;
    }
}
<?php declare(strict_types=1);

namespace App\Command;

use App\Dto\Movie\MovieDto;
use App\Entity\Movie;
use App\Service\Movie\MovieService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use Exception;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FetchDataCommand extends Command
{
    private const
        SOURCE = 'https://trailers.apple.com/trailers/home/rss/newtrailers.rss',
        COUNT = 10
    ;

    protected static $defaultName = 'fetch:trailers';

    private ClientInterface $httpClient;
    private LoggerInterface $logger;
    private EntityManagerInterface $doctrine;
    private DOMDocument $document;
    private MovieService $movieService;

    /**
     * FetchDataCommand constructor.
     *
     * @param ClientInterface $httpClient
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $em
     * @param MovieService $movieService
     * @param string|null $name
     */
    public function __construct(
        ClientInterface $httpClient,
        LoggerInterface $logger,
        EntityManagerInterface $em,
        MovieService $movieService,
        string $name = null
    ) {
        parent::__construct($name);
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $this->doctrine = $em;
        $this->movieService = $movieService;
        $this->document = new DOMDocument();
    }

    public function configure(): void
    {
        $this
            ->setDescription('Fetch data from iTunes Movie Trailers')
            ->addArgument('source', InputArgument::OPTIONAL, 'Overwrite source')
            ->addArgument('count', InputArgument::OPTIONAL, 'Overwrite count')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info(
            sprintf('Start %s at %s', __CLASS__, date_create()->format(DATE_ATOM))
        );

        $source = $input->getArgument('source') ?? self::SOURCE;
        $count = $input->getArgument('count') ?? self::COUNT;

        if (!is_string($source)) {
            throw new RuntimeException('Source must be string');
        }

        if (!is_integer($count)) {
            throw new RuntimeException('Count must be integer');
        }

        $io = new SymfonyStyle($input, $output);
        $io->title(sprintf('Fetch data from %s', $source));

        try {
            $response = $this->httpClient->sendRequest(new Request('GET', $source));
        } catch (ClientExceptionInterface $e) {
            throw new RuntimeException($e->getMessage());
        }

        if (($status = $response->getStatusCode()) !== 200) {
            throw new RuntimeException(sprintf('Response status is %d, expected %d', $status, 200));
        }

        $this->processXml($response->getBody()->getContents(), $count);

        $this->logger->info(sprintf('End %s at %s', __CLASS__, date_create()->format(DATE_ATOM)));

        return 0;
    }

    /**
     * @param string $data
     * @param int $countItem
     * @throws Exception
     */
    protected function processXml(string $data, int $countItem): void
    {
        $xml = (new SimpleXMLElement($data))->children();
        $namespace = $xml->getNamespaces(true)['content'];

        if (!property_exists($xml, 'channel')) {
            throw new RuntimeException('Could not find \'channel\' element in feed');
        }

        $items = $xml->xpath(sprintf('/rss/channel/item[position() <= %d]', $countItem));

        foreach ($items as $item) {
            $movieDto = new MovieDto();
            $movieDto->title = (string) $item->title;
            $movieDto->description = (string) $item->description;
            $movieDto->link = (string) $item->link;
            $movieDto->image = $this->parseImage((string) $item->children($namespace)->encoded);
            $movieDto->pubDate = $this->parseDate((string) $item->pubDate);

            $this->movieService->updateMovie($this->getMovie((string) $item->title), $movieDto);
        }
    }

    /**
     * @param string $date
     * @return DateTime
     * @throws Exception
     */
    protected function parseDate(string $date): DateTime
    {
        return new DateTime($date);
    }

    protected function parseImage(string $content)
    {
        @$this->document->loadHTML($content);
        $imageTags = $this->document->getElementsByTagName('img');
        return $imageTags[0]?->getAttribute('src');
    }

    protected function getMovie(string $title): Movie
    {
        $item = $this->doctrine->getRepository(Movie::class)->findOneBy(['title' => $title]);

        if ($item === null) {
            $this->logger->info('Create new Movie', ['title' => $title]);
            $item = new Movie();
        } else {
            $this->logger->info('Move found', ['title' => $title]);
        }

        if (!($item instanceof Movie)) {
            throw new RuntimeException('Wrong type!');
        }

        return $item;
    }
}

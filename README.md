Тестовое задание для PHP-программиста MIDDLE-уровень
====================================================

------------------------

#### Запуск проекта

- Запустите контейнеры: `docker-compose up -d`
- Установите PHP-зависимости: `docker-compose exec app composer install`
- Запустить инициализацию таблиц БД: `docker-compose exec app bin/console orm:schema-tool:update --force`
- Если БД уже была инициализирована и необходимо изменить её схему: `docker-compose exec app bin/console orm:clear-cache:metadata`
- Запустить команду для получения трейлеров: `docker-compose exec app bin/console fetch:trailers`
- Открыть браузер и перейти по ссылке: `http://localhost:8080`
- Остановка контейнеров: `docker-compose down`

Итог работы
-----------

#### Чек-лист

- Импорт в БД приложение 10 записей из iTunes Movie Trailers
- Главная страница проекта с трейлерами и детальная страница с подробной информацией о трейлере
- Добавлен сервис класс MovieService для управления сущностью Movie
- Изменен способ сохранения сущности Movie с использованием MovieDto и MovieService
- Добавлена реализация кнопки "Нравится" на подробной странице трейлера.
